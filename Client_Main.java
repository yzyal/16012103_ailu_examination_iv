import java.io.*;
import java.net.*;
import java.util.*;
public class Client_Main
{
	public static void main (String args[])
	{
		Socket mysocket;
		DataInputStream in=null;
		DataOutputStream out=null;
		try
		{
			mysocket = new Socket("127.0.0.1",2010);
			in = new DataInputStream(mysocket.getInputStream());
			out = new DataOutputStream(mysocket.getOutputStream());
			for (int i=0;i<10;i++)
			{
				Scanner scanner = new Scanner(System.in);	
				String source = scanner.nextLine();
				out.writeUTF(source);
				String s= in.readUTF();
				System.out.println("客户收到服务器的回答："+s);
				Thread.sleep(500);
			}
		}
		catch(Exception e)
		{
			System.out.println("服务器已断开"+e);
		}
	}
}